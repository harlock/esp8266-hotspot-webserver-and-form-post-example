# ESP8266 hotspot webserver and form POST example

Very simple example of creating a wifi hotspot with an ESP8266, serving a webpage with an input form, and using the data sent by the client.

This program does the following tasks :

* start a wifi hotspot
* serve a web page with an input field and a button
* when the button is presssed, submit the form with POST method
* get the data and send it to the serial port, and write it on an lcd display

## Circuit

![](../lcd-GDM1602K.png)

The lcd display is optional. You can use the ESP8266 alone and only check serial messages.

## Usage

* Once powered, the ESP8266 start a wifi hotspot, and blinks its onboard led to show it is ready
* with a computer or smartphone, connect to hotspot "ProutProut"
* with your web browser, open this link : [192.168.4.1](http://192.168.4.1)

![](../webpage.png)

* write any text in the input field, press the button
* The text will be sent by the ESP8266 to its usb-serial port and printed on the lcd display.
