#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <LiquidCrystal.h>

#define ESSID "ProutProut"

ESP8266WebServer server(80);
boolean b=0;
char page[] = "<!DOCTYPE HTML>"
	"<head><title>ESP8266 - Hotspot</title>"
	"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"></head>"
	"<html><span style=\"font-size:1.3em\">"
	"<form action='/' method='POST'>"
	"Message : "
	"<INPUT maxlength=16 size=16 name=X1><br><br>"
	"<INPUT TYPE='submit' VALUE='Send'>"
	"</form></span></html>";

LiquidCrystal lcd(D6, D5, D4, D3, D2, D1);

void handleRoot()
	{
	char message[17];
	server.send ( 200, "text/html", page);
	if ( server.hasArg("X1") ) 
		{
		strcpy(message,server.arg("X1").c_str());
		lcd.clear();
		lcd.setCursor(0,0);
  		lcd.print(message);
		Serial.println(message);
		}
	else lcd.clear();
	}

void setup()
	{
	pinMode(LED_BUILTIN, OUTPUT);
	
	lcd.begin(8,2);
  	lcd.clear();
  		
	Serial.begin(9600);
	Serial.println();

	WiFi.disconnect();
	WiFi.softAPdisconnect(true);
	
	// Start access point
	if (WiFi.softAP(ESSID))
		{
		Serial.println("HotSpot started.");
		Serial.print("Connect to ");
		Serial.print(ESSID);
		Serial.println(" wifi network");
		Serial.println("and open 192.168.4.1 in web browser.");
		}
	// I wonder it the following is usefull.
	// Could WiFi.softAP() fail ?
	else
		{
		Serial.println("Failed.");
		digitalWrite(LED_BUILTIN,HIGH); // switch off the led
		abort();
		}

	// Start web server
	server.on ( "/", handleRoot );
	server.begin();
  	}

void loop()
	{
	server.handleClient();
	b=1-b;	// blink builtin LED
	digitalWrite(LED_BUILTIN,b);
	delay(500);
	}
